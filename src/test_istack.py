from istack import IStack
import random


class TestIStack():
    data = [random.randint(0, 20) for i in range(5)]
    istack = IStack(max_size=len(data))

    def test_istack_push(self):
        value = self.istack.push(self.data[0])
        assert value == self.data[0]

    def test_istack_pop(self):
        self.istack.pop()
        assert self.istack.size() == 0

    def test_istack_is_empty(self):
        assert self.istack.is_empty() is True

    def test_istack_is_full(self):
        for value in self.data:
            self.istack.push(value)
        assert self.istack.is_full() is True

    def test_istack_size(self):
        assert self.istack.size() == len(self.data)

    def test_istack_contain(self):
        assert self.istack.contain(self.data[0])

    def test_istack_peek(self):
        assert self.istack.peek() == self.data[len(self.data) - 1]

    def test_clear(self):
        self.istack.clear()
        assert self.istack.size() == 0
