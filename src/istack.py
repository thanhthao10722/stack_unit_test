class IStack:
    def __init__(self, max_size):
        self._s = []
        self.max_size = max_size

    def size(self):
        return len(self._s)

    def clear(self):
        if not self.is_empty():
            for index in range(0, self.size()):
                self.pop()

    def push(self, value):
        self._s.append(value)
        return self.peek()

    def pop(self):
        if len(self._s) > 0:
            self._s.pop(len(self._s) - 1)
        else:
            raise IndexError

    def peek(self):
        if len(self._s) >= 1:
            return self._s[len(self._s) - 1]
        else:
            return "Empty list"

    def is_empty(self):
        if len(self._s) > 0:
            return False

        return True

    def is_full(self):
        if len(self._s) == self.max_size:
            return True
        return False

    def contain(self, value):
        if value in self._s:
            return True
        return False
